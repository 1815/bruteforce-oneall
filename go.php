<?php

require 'vendor/autoload.php';
/**
 * @var string $key
 * @var string $secret
 * @var string $api_url
 * @var string $email_prefix
 */
require 'config.php';

$amount     = 1;
$random_run = random_int( 0, PHP_INT_MAX );

$users = [];
for ( $i = 0; $i < $amount; $i ++ ) {
	$email = 'brute_' . $email_prefix . '_' . $random_run . '_' . trim( substr( str_shuffle( '0123456789abcdefghijklmnopqrstuvwxyz._' ), 0, 12 ), '._' ) . '@1815.nl';

	if ( isset( $users[ $email ] ) ) {
		$i --;
		continue;
	}

	$users[ $email ] = [
		'login'    => $email,
		'identity' => [
			'emails' => [
				[
					'value'       => $email,
					'is_verified' => true,
				],
			],
		],
	];
}

echo count( $users ) . ' unique users generated' . PHP_EOL;

$bearer = 'Basic ' . base64_encode( $key . ':' . $secret );
$client = new \GuzzleHttp\Client( [
	'base_uri' => $api_url,
	'headers'  => [
		'Content-Type'  => 'application/json',
		'Accept'        => 'application/json',
		'Authorization' => $bearer,
	],
] );

$requests = [];

function requests( $users ): Generator {
	foreach ( $users as $email => $user ) {
		yield new \GuzzleHttp\Psr7\Request( 'POST', '/storage/users/user/lookup.json', [], json_encode( [
			'request' => [
				'user' => [
					'login' => $email,
				],
			],
		] ) );

		yield new \GuzzleHttp\Psr7\Request( 'POST', '/storage/users.json', [], json_encode( [
			'request' => [
				'user' => $user,
			],
		] ) );
	}
}

$tokens = [];
$failed = [];
$pool   = new \GuzzleHttp\Pool( $client, requests( $users ), [
	'concurrency' => 1,
	'fulfilled'   => function ( \GuzzleHttp\Psr7\Response $response, $index ) use ( &$tokens ) {
		$data = json_decode( (string) $response->getBody(), true );

		$tokens[] = $data['response']['result']['data']['user']['user_token'];
	},
	'rejected'    => function ( $reason, $index ) use ( &$failed ) {
		var_dump( $reason->getMessage() );
	},
] );

$promise = $pool->promise();

$promise->wait();

var_export($tokens);
